package controller;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import dominio.Pedido;

/**
 * Classe respons�vel por gerenciar os pedidos da aplica��o
 * 
 * @author janioxavier
 *
 */
public class ListaPedido {
	private ArrayBlockingQueue<Pedido> pedidos;
	private int limitePedidos;
	private int quantidadePedidos;
	private int pedidosProduzidosConsumidos;
	private int limiteProducao;
	private final Lock lock = new ReentrantLock();
	private final Condition notFull = lock.newCondition();
	private final Condition notEmpty = lock.newCondition();

	/**
	 * Construtor padr�o que inicializa a quantidade m�xima de pedidos
	 * 
	 * @param limite
	 */
	public ListaPedido(int limite, int limiteProducao) {
		pedidos = new ArrayBlockingQueue<>(limite);
		this.limitePedidos = limite;
		quantidadePedidos = 0;
		pedidosProduzidosConsumidos = 0;
		this.limiteProducao = limiteProducao;
	}

	/**
	 * insere um pedido na lista de pedidos
	 * 
	 * @param pedido
	 *            Um novo pedido
	 * @throws InterruptedException
	 *             se a thread atual for interrompida.
	 */
	public void inserir(Pedido pedido) throws IllegalStateException {
		pedidos.add(pedido);
		quantidadePedidos++;
		pedidosProduzidosConsumidos++;

	}

	/**
	 * remove um pedido da lista de pedidos
	 * 
	 * @return um pedido da lista de pedidos
	 * @throws InterruptedException
	 *             se a thread for interrompida
	 */
	public Pedido remover() {

		Pedido pedido = null;
		try {
			pedido = pedidos.take();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pedidosProduzidosConsumidos++;
		quantidadePedidos--;
		return pedido;
	}

	public boolean temPedidos() {
		return quantidadePedidos > 0;
	}

	/**
	 * obt�m o limite de pedidos
	 * 
	 * @return limite de pedidos que podem ser armazenados
	 */
	public int getLimitePedidos() {
		return limitePedidos;
	}

	/**
	 * obt�m a quantidade de pedidos armazenados atualmente
	 * 
	 * @return a quantidade de pedidos armazenados
	 */
	public int getQuantidadePedidos() {
		return quantidadePedidos;
	}

	/**
	 * Remove todos os pedidos criados.
	 */
	public void limparPedidos() {
		pedidos.clear();
		pedidosProduzidosConsumidos = 0;
		quantidadePedidos = 0;
	}

	/**
	 * @return true se o limite maximo de pdidos foi alcançado
	 */
	public boolean alcancouLimite() {
		return pedidosProduzidosConsumidos >= limiteProducao;
	}
}
