package controller;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.TimeUnit;

import util.Log;
import util.Relatorio;

/**
 * Classe respons�vel por inicializar o processamento dos pedidos, computando o
 * tempo de cada execu��o.
 * 
 * @author janioxavier
 *
 */
public class Simulador {
	private List<Integer> quantidadeDeAgente;
	private SistemaPedido sistema;
	private Log log;
	private Stack<String> pilhaLog;
	private final String caminhoArquivo = "resources/etapa1/";
	private Relatorio relatorio;

	/**
	 * Inicializa os dados da classe.
	 * 
	 * @param limitePedido
	 *            quantidade m�xima de pedidos
	 * @param quantidadeDeAgente
	 *            lista com as quantidades de agentes
	 */
	public Simulador(int limitePedido, List<Integer> quantidadeDeAgente) {
		super();
		this.quantidadeDeAgente = quantidadeDeAgente;
		this.log = new Log();
		this.sistema = new SistemaPedido(limitePedido, log);
		pilhaLog = new Stack<>();
		relatorio = new Relatorio();
	}

	public void init(int numExecucoes) {
		quantidadeDeAgente.forEach((x) -> {
			String resultado;
			List<Long> tempoExecucao;
			relatorio.addQuantidade(x);
			tempoExecucao = computarTempoProcessamento(numExecucoes, x);
			resultado = Relatorio.imprimirResultados(numExecucoes, x, tempoExecucao);
			// escreverResultado(resultado, x);
			relatorio.addResultadoAgente(resultado, x);
			// log.mostrarTelaLog("Log da Execucao com " + x + " agentes",
			// resultado);
		});
	}

	/**
	 * computa o tempo de processamento de n execu��es para uma dada quantidade
	 * de agentes
	 * 
	 * @param numExecucoes
	 *            numero de execucoes
	 * @param quantidadeAgente
	 *            quantidade de agentes
	 * @return uma lista com o tempo de Execucao
	 */
	public List<Long> computarTempoProcessamento(int numExecucoes, int quantidadeAgente) {
		long t0;
		long t1;
		List<Long> tempoExecucao = new ArrayList<>();

		for (int i = 0; i < numExecucoes; i++) {
			log.escreverLinha("Execucao " + (i + 1) + "\n");
			
			// tempo inicial
			t0 = System.nanoTime();

			// processamento
			sistema.initProcessamento(quantidadeAgente);

			// tempo final
			t1 = System.nanoTime();

			// escrever no log
			relatorio.addLogExecucao(log.getLog(), i + 1, quantidadeAgente);

			// limpar log;
			log.zerarLog();
			
			// salvar tempo
			tempoExecucao.add(TimeUnit.NANOSECONDS.toMillis(t1 - t0));
		}
		return tempoExecucao;
	}

	public static void main(String[] args) {
		Integer[] agentes = new Integer[] { 1, 5, 10, 50, 100, 500, 1000, };

		Simulador sim = new Simulador(5000, Arrays.asList(agentes));
		sim.init(10);
	}
}
