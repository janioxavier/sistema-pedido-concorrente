package controller;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import dominio.AgenteConsumidor;
import util.Log;

public class GerenciadorConsumidor {
	private List<Callable<Object>> consumidores;
	private ExecutorService executor;

	public GerenciadorConsumidor(int quantidade, ListaPedido pedidos, Log log) {
		consumidores = new ArrayList<>();
		for (int i = 0; i < quantidade; i++) {
			consumidores.add(Executors.callable(new AgenteConsumidor(i, pedidos, log)));
		}
		executor = Executors.newFixedThreadPool(quantidade);
	}
	
	public void iniciarConsumidores() {
		try {
			executor.invokeAll(consumidores);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void terminarConsumidores() {
		executor.shutdown();
	}
}
