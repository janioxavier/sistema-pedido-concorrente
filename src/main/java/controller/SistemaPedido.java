package controller;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import dominio.AgenteConsumidor;
import dominio.AgenteProdutor;
import util.Log;

/**
 * Classe respons�vel por simular o processamento dos pedidos.
 * 
 * @author janioxavier
 *
 */
public class SistemaPedido {
	private Log log;
	private ListaPedido pedidos;
	private GeradorPedido gerador;

	/**
	 * Inicializa a quantidade maxima de pedidos.
	 * 
	 * @param quantidadePedido
	 */
	public SistemaPedido(int quantidadePedido, Log log) {
		super();
		this.pedidos = new ListaPedido(quantidadePedido, 5000);
		this.gerador = new GeradorPedido();
		this.log = log;
	}

	/**
	 * inicializa o processamento de pedidos com dada quantidade de agentes
	 * 
	 * @param quantidadeAgente
	 */
	public void initProcessamento(int quantidadeAgente) {
		// reinicializa o gerador de pedidos
		gerador.reiniciarGerador();
		// remove os pedidos criados
		pedidos.limparPedidos();

		// inicializa as threads dos agentes
		initAgentes(quantidadeAgente);
	}

	/**
	 * Inicializa as threads dos agentes que consumirao e produzirao os pedidos
	 * 
	 * @param quantidadeAgente
	 */
	public void initAgentes(int quantidadeAgente) {
		ExecutorService executor = Executors.newFixedThreadPool(quantidadeAgente * 2);

		List<Callable<Object>> calls = new ArrayList<>();
		for (int i = 0; i < quantidadeAgente; i++) {
			if (i % 2 == 0) {
				calls.add(Executors.callable(new AgenteProdutor(i, pedidos, gerador, log)));
			} else {
				calls.add(Executors.callable(new AgenteConsumidor(i, pedidos, log)));
			}
		}
		try {
			executor.invokeAll(calls);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		executor.shutdown();
	}
}
