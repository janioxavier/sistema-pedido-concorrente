package controller;

import java.math.BigInteger;
import java.util.Random;

import dominio.Pedido;

/**
 * Classe respons�vel por gerar pedidos.
 * @author janioxavier
 *
 */
public class GeradorPedido {
	
	private BigInteger id;
	private Random random;
	
	/**
	 * Construtor padr�o
	 */
	public GeradorPedido() {
		id = BigInteger.ZERO;
		random = new Random();
	}
	
	private void incrementarId() {
		id = id.add(BigInteger.ONE);
	}
	
	/**
	 * zera o gerador de pedidos
	 */
	public void reiniciarGerador() {
		id = BigInteger.ZERO;
	}
	
	private String gerarIdCom(int digitos) {
		BigInteger number = id;
		incrementarId();
		return String.format("%0"+digitos+"d", number.intValueExact());
	}
	
	/**
	 * gera um pedido �nico.
	 *@return um pedido �nico
	 */
	public synchronized Pedido gerarPedido() {
		
		Pedido pedido = new Pedido(gerarIdCom(20), gerarMensagem(1000));
		
		return pedido;
	}

	private String gerarMensagem(int tamanho) {
		String mensagem = ""; 
		tamanho = random.nextInt(tamanho);
		for (int i = 0; i < tamanho; i++) {
			mensagem += Character.toString((char) random.nextInt(256)) + " ";
		}
		return mensagem;
	}
}
