package util;

import java.time.Instant;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

/**
 * Classe respons�vel por armazenar o tempo atual.
 * 
 * @author janioxavier
 *
 */
public class Tempo {
	private Instant tempo;

	public Tempo() {
		tempo = Instant.now();
	}
	
	public Tempo(Tempo tempoFinal) {
		this.tempo = tempoFinal.getTempo();
	}

	public Instant getTempo() {
		return tempo;
	}
	
	/**
	 * verifica se dado tempo é maior que o tempo deste objeto
	 * @param tempo dado tempo
	 * @return true se o tempo for maior e false caso contrário.
	 */
	public boolean ehMaior(Tempo tempo) {
		return this.tempo.compareTo(tempo.getTempo()) > 0;
	}
	
	public void setTempo(Instant tempo) {
		this.tempo = tempo;
	}

	public static String getTempoFormatado(LocalTime time) {
		return time.format(DateTimeFormatter.ofPattern("hh:mm:ss"));
	}

	public String getTempoFormatado() {
		return OffsetDateTime.ofInstant(tempo, ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern("hh:mm:ss"));
	}

	public String addTempo(long quantidade) {
		tempo = ChronoUnit.SECONDS.addTo(tempo, quantidade);
		return getTempoFormatado();
	}
}
