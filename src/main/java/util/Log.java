package util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Log {
	private String log;
	private boolean comGUI;
	private JFrame janela;
	private JPanel painel;
	private JTextArea areaTexto;
	private JScrollPane scroll;
	
	public Log(boolean comGUI) {
		this.comGUI = comGUI;
		if (comGUI) {
			initComponent();	
		}
		log = "";
	}
	
	public Log() {
		log = "";
	}
	
	public void initComponent() {
		janela = new JFrame("Log de Atividades");
		painel = new JPanel();
		areaTexto = new JTextArea();
		scroll = new JScrollPane(areaTexto);
		
		painel.setSize(600, 700);
		
		areaTexto.setEditable(false);
		scroll.setSize(450, 600);
		janela.setSize(500, 700);
		janela.add(scroll);
		janela.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		janela.setVisible(true);		
	}

	public synchronized void escreverLinha(String msg) {
		log += msg + "\n";
		if (comGUI) {
			//areaTexto.setText(log);
		} else {
			//System.out.println(msg);
		}
	}
	
	public void mostrarTelaLog(String title, String msg) {
		initComponent();
		janela.setTitle(title);
		areaTexto.setText(msg + "\n\n" + log);		
	}
	
	public String getLog() {
		return log;
	}
	
	public void zerarLog() {
		log = "";
	}
	
}
