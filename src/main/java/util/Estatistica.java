package util;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class Estatistica {
	
	public static double media(List<Long> lista) {
		double media = 0;
		for(Long valor : lista) {
			media += valor.doubleValue();
		}
		media /= lista.size();
		return (Double) media;			
	}
	
	public static <T> double variancia(List<Long> lista) {
		double media = Estatistica.media(lista);
		double variancia = 0;
		for (Long valor : lista) {
			variancia += Math.pow((valor.doubleValue() - media), 2);
		}
		variancia /= lista.size();		
		return variancia; 
	}
	
	public static <T> double desvioPadrao(List<Long> lista) {
		double variancia = variancia(lista);
		
		double desvioPadrao = Math.sqrt(variancia);
		
		return desvioPadrao;
	}
	
	public static Long maximo(List<Long> lista) {		
		return Collections.max(lista);
	}
	
	public static Long minimo(List<Long> lista) {
		return Collections.min(lista);
	}
	
	
	
	
}
