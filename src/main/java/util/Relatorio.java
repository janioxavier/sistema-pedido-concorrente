package util;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;

/**
 * classe respons�vel por imprimir os resultados da aplica��o
 * 
 * @author janioxavier
 *
 */
public class Relatorio {
	private JButton[] buttons;
	private JButton[] buttonsAgente;
	private JToolBar toolBar;
	private JScrollPane scrollPane;
	private JTextArea areaTexto;
	private JFrame janela;
	private Box vBox;
	private Box hBox;
	private ButtonGroup grupoBotao;
	private int botaoSelecionadoAgente = 1;
	private Map<Integer, String> resultadoAgente;
	private Map<Integer, List<JButton>> agenteButaoExecucao;
	private Map<Integer, Map<Integer, String>> quantidadeExecucaoLog;
	private Map<Integer, Boolean> resultadoPreparado; 

	public Relatorio() {
		resultadoAgente = new HashMap<>();
		quantidadeExecucaoLog = new HashMap<>();
		agenteButaoExecucao = new HashMap<>();
		resultadoPreparado = new HashMap<>();
		initComponent();
	}

	public void addQuantidade(int quantidade) {
		JRadioButton butao = new JRadioButton("" + quantidade);
		resultadoPreparado.put(quantidade, false);
		grupoBotao.add(butao);
		agenteButaoExecucao.put(quantidade, new ArrayList<>());
		butao.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// salva botao selecionado
				botaoSelecionadoAgente = Integer.parseInt(butao.getText());
				int chave = botaoSelecionadoAgente;
				JButton primeiroBotao = null;
				// mostra os botoes relacionados a execucao
				if (agenteButaoExecucao.containsKey(chave)) {					
					toolBar.removeAll();
					List<JButton> butoes = agenteButaoExecucao.get(chave);
					for (JButton b : butoes) {
						toolBar.add(b);
					}
					
					// recupera o primeiro botao
					if (butoes.size() > 0) {						
						primeiroBotao = butoes.get(0);
					}						
				}
				
				// mostra o resultado de todas as execucoes
				if (resultadoPreparado.get(chave)) {
					areaTexto.setText(resultadoAgente.get(botaoSelecionadoAgente));
				}
				// mostra o resultado da primeira execucao se tiver
				else if (primeiroBotao != null) {
					int primeiroValor = Integer.parseInt(primeiroBotao.getText());
					areaTexto.setText(quantidadeExecucaoLog.get(chave).get(primeiroValor));
					// mostra mensagem informando que o resultado ainda nao foi processado
			        JOptionPane.showMessageDialog(null,"O resultado ainda n�o foi processado para o n�mero de agentes: : " + botaoSelecionadoAgente);
					
				}
				// mostra mensagem informando que nenhuma execucao foi terminada 
				else {
			        JOptionPane.showMessageDialog(null,"Nenhum log de execucao ainda foi preparado para o n�mero de agentes: " + botaoSelecionadoAgente);
				}
				
				
			}

		});
		hBox.add(butao);
		quantidadeExecucaoLog.put(quantidade, new HashMap<>());
		janela.setVisible(true);
	}

	public void addLogExecucao(String log, int execucao, int quantidade) {
		// coloca para cada quantidade de agente um mapa que mapeia para
		// cada execucao um log
		quantidadeExecucaoLog.get(quantidade).put(execucao, log);
		JButton butao = new JButton("" + execucao);
		butao.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int chaveExecucaoLog = botaoSelecionadoAgente;
				
				int execucaoSelecionada = Integer.parseInt(butao.getText());
				Map<Integer, String> execucaoLog = quantidadeExecucaoLog.get(chaveExecucaoLog);
				if (execucaoLog != null) {
					areaTexto.setText(execucaoLog.get(execucaoSelecionada));
				}
			}
		});
		// adiciona o butao a lista de butoes
		agenteButaoExecucao.get(quantidade).add(butao);
		//toolBar.add(butao);
		janela.setVisible(true);
	}

	public void addResultadoAgente(String resultado, int quantidade) {
		resultadoAgente.put(quantidade, resultado);
		resultadoPreparado.put(quantidade, true);
		JButton butao = new JButton("resultado");
		butao.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				areaTexto.setText(resultadoAgente.get(botaoSelecionadoAgente));
			}
		});
		// adiciona o butao a lista de butoes
		agenteButaoExecucao.get(quantidade).add(butao);	
	}

	private void initComponent() {
		areaTexto = new JTextArea();
		scrollPane = new JScrollPane(areaTexto);
		janela = new JFrame();
		toolBar = new JToolBar();
		grupoBotao = new ButtonGroup();
		vBox = Box.createVerticalBox();
		hBox = Box.createHorizontalBox();

		vBox.add(hBox);
		vBox.add(toolBar);
		janela.getContentPane().add(vBox, BorderLayout.NORTH);
		janela.getContentPane().add(scrollPane, BorderLayout.CENTER);

		janela.setSize(800, 500);
		janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		janela.setVisible(true);
	}

	public static String imprimirResultados(int numExecucoes, int numAgentes, List<Long> tempoExecucao) {
		String resultado = "";
		resultado = "Quantidade de Execucoes: " + numExecucoes + "\n";
		resultado += "Numero de Agentes: " + numAgentes + "\n";
		resultado += "Tempo m�nimo: " + Estatistica.minimo(tempoExecucao) + "\n";
		resultado += "Tempo m�dio: " + Estatistica.media(tempoExecucao) + "\n";
		resultado += "Tempo maximo: " + Estatistica.maximo(tempoExecucao) + "\n";
		resultado += "Desvio Padrão: " + String.format("%.2f", Estatistica.desvioPadrao(tempoExecucao)) + "\n";
		System.out.println(resultado);
		return resultado;
	}

	public static void main(String[] args) {

	}
}
