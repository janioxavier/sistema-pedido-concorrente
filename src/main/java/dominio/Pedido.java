package dominio;

import util.Tempo;

public class Pedido {
	private char[] id = new char[20];
	private char[] dados = new char[1000];
	private Tempo tempoFinal;
	
	public Pedido(String id, String dados) {
		this.id = id.toCharArray();
		this.dados = dados.toCharArray();
	}
	
	public String getId() {
		return new String(id);
	}
	
	public String getDados() {
		return new String(dados);
	}
	
	public void setTempoFinal(Tempo tempoFinal) {
		this.tempoFinal = new Tempo(tempoFinal);
	}
	
	public Tempo getTempoFinal() {
		return tempoFinal;
	}
}
