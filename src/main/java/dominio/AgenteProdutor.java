package dominio;

import controller.GeradorPedido;
import controller.ListaPedido;
import util.Log;
import util.Tempo;

/**
 * Classe resons�vel por consumir os pedidos da lista de pedidos.
 * 
 * @author janioxavier
 *
 */
public class AgenteProdutor implements Runnable {
	private ListaPedido pedidos;
	private Tempo tempo;
	private Log log;
	private int id;
	private GeradorPedido gerador;

	public AgenteProdutor(int id, ListaPedido pedidos, GeradorPedido gerador, Log log) {
		this.id = id;
		this.pedidos = pedidos;
		this.log = log;
		this.gerador = gerador;
		tempo = new Tempo();
	}

	@Override
	public void run() {
		while (!pedidos.alcancouLimite()) {
			String idPedido;
			String tempoStr;
			Pedido pedido = gerador.gerarPedido();
			if (pedido != null) {
				tempoStr = tempo.getTempoFormatado();
				idPedido = pedido.getId();
				log.escreverLinha(tempoStr + " agente PRODUTOR " + id + " INICIA pedido " + idPedido);
				tempoStr = tempo.addTempo(3);
				log.escreverLinha(tempoStr + " agente PRODUTOR " + id + " TERMINA pedido " + idPedido);
				pedido.setTempoFinal(tempo);
				while (true) {
					try {
						pedidos.inserir(pedido);
						break;
					} catch (IllegalStateException e) {
						// TODO Auto-generated catch block
						continue;
					}
				}
			}
		}
	}
}
