package dominio;

import controller.ListaPedido;
import util.Log;
import util.Tempo;

/**
 * Classe resons�vel por consumir os pedidos da lista de pedidos.
 * 
 * @author janioxavier
 *
 */
public class AgenteConsumidor implements Runnable {
	private ListaPedido pedidos;
	private Log log;
	private int id;
	private Tempo tempo;

	public AgenteConsumidor(int id, ListaPedido pedidos, Log log) {
		this.id = id;
		this.pedidos = pedidos;
		this.log = log;
		tempo = new Tempo();
	}

	@Override
	public void run() {
		while (!pedidos.alcancouLimite()) {
			String idPedido;
			String tempoStr;
			Pedido pedido = null;
				pedido = pedidos.remover();
		
			if (pedido != null) {
				Tempo tempoPedido = pedido.getTempoFinal();
				// verifica se o tempo do pedido é menor que o tempo atual
				if (tempoPedido.ehMaior(tempo)) {  
					tempo.setTempo(tempoPedido.getTempo());
					tempoStr = tempo.getTempoFormatado();
					idPedido = pedido.getId();
					log.escreverLinha(tempoStr + " agente CONSUMIDOR " + id + " INICIA pedido " + idPedido);
					tempoStr = tempo.addTempo(3);
					log.escreverLinha(tempoStr + " agente CONSUMIDOR " + id + " TERMINA pedido " + idPedido);
				} else {
					tempoStr = tempo.getTempoFormatado();
					idPedido = pedido.getId();
					log.escreverLinha(tempoStr + " agente CONSUMIDOR " + id + " INICIA pedido " + idPedido);
					tempoStr = tempo.addTempo(3);
					log.escreverLinha(tempoStr + " agente CONSUMIDOR " + id + " TERMINA pedido " + idPedido);
				
				}
			}
		}
	}
}
