package controller;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GeradorPedidoTest {
	private GeradorPedido gerador;

	@Before
	public void setUp() throws Exception {
		gerador = new GeradorPedido();
	}

	@Test
	public void gerarPedidotest() {
		String id;
		for (int i = 0; i < 5000; i++) {
			id = gerador.gerarPedido().getId();
			//System.out.println(id);
			assertEquals(20, id.length());
		}

	}

}
