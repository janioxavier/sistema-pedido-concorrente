package controller;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import dominio.Pedido;

public class ListaPedidoTest {
	
	private ListaPedido pedidos;
	private final int limite = 5000;

	@Before
	public void setUp() throws Exception {
		pedidos = new ListaPedido(limite, limite);
	}

	@Test
	public void testInserir() throws InterruptedException {
		for (int i = 0; i < limite; i++) {
			pedidos.inserir(new Pedido(""+i,"123456asc"));
			assertEquals(i+1, pedidos.getQuantidadePedidos());
		}
	}

	@Test
	public void testRemover() throws InterruptedException {
		// teste remover vazio
		//assertEquals(null, pedidos.remover());
		
		Pedido pedido = new Pedido("1", "123");
		
		// teste remover nao vazio
		pedidos.inserir(pedido);
		
		
		assertEquals(pedido, pedidos.remover());
	}

}
