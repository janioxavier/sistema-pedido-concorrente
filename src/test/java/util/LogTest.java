package util;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class LogTest {
	
	private Log log;
	
	@Before
	public void setUp() throws Exception {
		log = new Log(true);
	}

	@Test
	public void testEscreverLinha() throws InterruptedException {
		log.escreverLinha("oi");
		Thread.sleep(1000);
		log.escreverLinha("opabauquemequita");
		Thread.sleep(1000);
		log.escreverLinha("oi");
		Thread.sleep(1000);
		log.escreverLinha("oi");
		log.escreverLinha("oi");
		
	}

}
