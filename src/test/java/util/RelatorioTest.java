package util;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class RelatorioTest {
	private static List<Long> valores;

	static {
		Long[] numeros = new Long[]{
			 10L, 9L, 11L, 12L, 8L
		};
		valores = Arrays.asList(numeros);
	}
	@Test
	public void testImprimirResultados() {
		Relatorio.imprimirResultados(10, 5, valores);
	}

}
