package util;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class EstatisticaTest {
	
	private static List<Long> valores;

	static {
		Long[] numeros = new Long[]{
			 10L, 9L, 11L, 12L, 8L
		};
		valores = Arrays.asList(numeros);
	}

	@Test
	public void testMedia() {
		assertEquals(10.0, Estatistica.media(valores), 0.001);
	}

	@Test
	public void testVariancia() {
		assertEquals(2.0, Estatistica.variancia(valores), 0.01);
	}

	@Test
	public void testDesvioPadrao() {
		assertEquals(1.41, Estatistica.desvioPadrao(valores), 0.01);
	}

	@Test
	public void testMaximo() {
		long max = Estatistica.maximo(valores);
		assertEquals( 12, max);
	}

	@Test
	public void testMinimo() {
		long min = Estatistica.minimo(valores);
		assertEquals(8, min);
	}

}
